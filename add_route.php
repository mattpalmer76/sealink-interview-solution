<h1>Add or remove routes</h1>

<?php
    include_once 'functions.php';
    
    connect_db();

    if(isset($_POST['submit'])) {
        // go though each record and update
        $count = $_POST['count'];
        for($i = 0; $i <= $count; $i++) {
            if(isset($_POST['id-' .$i])) {
                $id = $_POST['id-' .$i];
                if(isset($_POST['delete-' .$id])) {
                    // attempt a delete
                    $deleted = delete($id, Route::TABLE);
                    // if cannot delete, it's likely linked to another record
                    if(!$deleted) {
                        echo '<p>Could not delete "' .$_POST['name-' .$id] .'" (id ' .$id .'). It is likely this record is used in another record.</p>';
                    }
                } else {
                    // update data
                    Route::upsert($_POST['name-' .$id], $_POST['line-id-' .$id], strval($id));
                }
            }
            
        }
        
        // for adding a new record
        if(!empty($_POST['name-new']) || !empty($_POST['model-new'])) {
            // insert new route
            Route::upsert($_POST['name-new'], $_POST['line-id-new']);
        }

    }

    $routes = Route::getAllRoutes();
    $lines = Line::getAllLines();

    ?>
        <form method="post" action="add_route.php">
            <?php
                // display each route
                foreach($routes as $route) {
                    $id = $route->id_route;
                    ?>
                        <p>ID: <?php echo $id ?></p>
                        <input type="hidden" name="id-<?php echo $id; ?>" value="<?php echo $id ?>"/>
                        <p>Name: <input type="text" name="<?php echo "name-$id"; ?>" value="<?php echo $route->name; ?>"></p>
                        <p>Lines: 
                            <select name="line-id-<?php echo $id; ?>">
                                <?php
                                    // list all lines
                                    foreach($lines as $line) {
                                        ?>
                                            <option value="<?php echo $line->id_line; ?>"<?php
                                                if($line->id_line === $route->line_id) {
                                                    echo ' selected="selected" ';
                                                }
                                            ?>>
                                                <?php echo $line->name; ?>
                                            </option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </p>
                        <p>Delete? <input type="checkbox" name="<?php echo "delete-$id"; ?>"></p>
                        <hr>
                    <?php
                }
            ?>
            <p>New entry - enter data to insert a new route</p>
            <p>Name: <input type="text" name="name-new"></p>
            <p>Lines: 
                <select name="line-id-new">
                    <?php
                        // list all lines
                        foreach($lines as $line) {
                            ?>
                                <option value="<?php echo $line->id_line; ?>">
                                    <?php echo $line->name; ?>
                                </option>
                            <?php
                        }
                    ?>
                </select>
            </p>
            <hr>

            <input type="hidden" name="count" value="<?php echo get_latest_id(Route::TABLE); ?>">
            <input type="submit" value="Apply changes" name="submit" />
            <a href="index.php">Back to home</a>
        </form>
    <?php
