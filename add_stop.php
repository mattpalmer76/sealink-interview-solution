<h1>Add or remove stops</h1>

<?php
    include_once 'functions.php';
    
    connect_db();

    if(isset($_POST['submit'])) {
        // go though each record and update
        $count = $_POST['count'];
        for($i = 0; $i <= $count; $i++) {
            if(isset($_POST['id-' .$i])) {
                $id = $_POST['id-' .$i];
                if(isset($_POST['delete-' .$id])) {
                    // attempt a delete
                    $deleted = delete($id, Stop::TABLE);
                    // if cannot delete, it's likely linked to another record
                    if(!$deleted) {
                        echo '<p>Could not delete "' .$_POST['name-' .$id] .'" (id ' .$id .'). It is likely this record is used in another record.</p>';
                    }
                } else {
                    // update data
                    Stop::upsert($_POST['name-' .$id], $_POST['coords-' .$id], $_POST['route-id-' .$id], strval($id));
                }
            }
            
        }
        
        // for adding a new record
        if(!empty($_POST['name-new']) || !empty($_POST['coords-new'])) {
            // insert new stop
            Stop::upsert($_POST['name-new'], $_POST['coords-new'], $_POST['line-id-new']);
        }

    }

    $stops = Stop::getAllStops();
    $routes = Route::getAllRoutes();

    ?>
        <form method="post" action="add_stop.php">
            <?php
                // display all stops
                foreach($stops as $stop) {
                    $id = $stop->id_stop;
                    ?>
                        <p>ID: <?php echo $id ?></p>
                        <input type="hidden" name="id-<?php echo $id; ?>" value="<?php echo $id ?>"/>
                        <p>Code: <input type="text" name="<?php echo "name-$id"; ?>" value="<?php echo $stop->name; ?>"></p>
                        <p>Coordinates: <input type="text" name="<?php echo "coords-$id"; ?>" value="<?php echo $stop->coords; ?>"></p>
                        <p>Routes: 
                            <select name="route-id-<?php echo $id; ?>">
                                <?php
                                    // list routes
                                    foreach($routes as $route) {
                                        ?>
                                            <option value="<?php echo $route->id_route; ?>" <?php
                                                if($route->id_route === $stop->route_id) {
                                                    echo ' selected="selected" ';
                                                }
                                            ?> >
                                                <?php echo $route->name; ?>
                                            </option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </p>
                        <p>Delete? <input type="checkbox" name="<?php echo "delete-$id"; ?>"></p>
                        <hr>
                    <?php
                }
            ?>
            <p>New entry - enter data to insert a new stop</p>
            <p>Code: <input type="text" name="name-new"></p>
            <p>Coordinates: <input type="text" name="coords-new"></p>
            <p>Lines: 
                <select name="line-id-new">
                    <?php
                        // list routes
                        foreach($routes as $route) {
                            ?>
                                <option value="<?php echo $route->id_route; ?>">
                                    <?php echo $route->name; ?>
                                </option>
                            <?php
                        }
                    ?>
                </select>
            </p>
            <hr>

            <input type="hidden" name="count" value="<?php echo get_latest_id(Stop::TABLE); ?>">
            <input type="submit" value="Apply changes" name="submit" />
            <a href="index.php">Back to home</a>
        </form>
    <?php
