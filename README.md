SpeedCo solution.
(Matt Palmer)

This is my submission for the SpeedCo bus system. It is a bare-bones PHP app accessing a mySQL database.

To get up and running, you will need to download XAMPP. (I am assuming you are using a Windows PC).

You can download it from here - https://www.apachefriends.org/index.html

Install XAMPP with all default settings

Once you have installed XAMPP, find out which directory it is in. It is most likely in C:\XAMPP.

Open command prompt and go to XAMPP\htdocs. If XAMPP is on C:, type the following...

c:
cd \xamppp\htdocs

Once you have done this, type...

git clone https://mattpalmer76@bitbucket.org/mattpalmer76/sealink-interview-solution.git

This will install the code onto your computer.

Next step is to start XAMPP.

In Windows Explorer, navigate to the XAMPP directory (most likely on C and in \XAMPP). Click on 'xampp-control'.

You will be presented with a control panel. Listed are five services. Click start on 'Apache' and 'MySQL'.

Now open your favourite web browser and enter this url:
http://localhost/sealink-interview-solution/

You will be presented with the index page of the system. The database will be created automatically and demonstation data will also be inserted.

Thanks for your time!