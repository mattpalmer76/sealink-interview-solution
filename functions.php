<?php
    include_once 'database_helper.php';
    include_once './model/bus.php';
    include_once './model/line.php';
    include_once './model/route.php';
    include_once './model/stop.php';
    include_once './model/schedule.php';

    $conn = null;
    $dh = null;

    // connect to the datbase and store connection as a global
    function connect_db() {
        $GLOBALS["dh"] = new DatabaseHelper();
        $GLOBALS["dh"]->create_database_if_not_exist();
    }

    // gets the highest PK ID in a table
    function get_latest_id($table) {
        return $GLOBALS["dh"]->last_id($table);
    }

    // check if record exists
    function check_record_exists($id, $table) {
        $result = $GLOBALS["dh"]->conn->query("SELECT * FROM $table WHERE `ID` = $id");

        return $result->num_rows > 0;
    }

    // delete a record
    function delete($id, $table) {
        $statement = "DELETE FROM $table WHERE `id` = $id";
        $result = $GLOBALS["dh"]->conn->query($statement);
        return $result === true;
    }