<h1>Add or remove schedules</h1>

<?php
    include_once 'functions.php';

    connect_db();

    if(isset($_POST['submit'])) {
        // go though each record and update
        $count = $_POST['count'];
        for($i = 0; $i <= $count; $i++) {
            if(isset($_POST['id-' .$i])) {
                $id = $_POST['id-' .$i];
                if(isset($_POST['delete-' .$id])) {
                    // attempt a delete
                    delete($id, Schedule::TABLE);
                } else {
                    // update data
                    Schedule::upsert($_POST['time-' .$id], $_POST['bus-id-' .$id], $_POST['line-id-' .$id], $_POST['name-' .$id], strval($id));
                }
            }
        }
        
        // for adding a new record
        if(!empty($_POST['name-new']) || !empty($_POST['model-new'])) {
            // insert new schedule
            Schedule::upsert($_POST['time-new'], $_POST['bus-id-new'], $_POST['line-id-new'], $_POST['name-new']);
        }

    }

    $schedules = Schedule::getAllSchedules();
    $buses = Bus::getAllBuses();
    $lines = Line::getAllLines();

    ?>
        <form method="post" action="add_schedule.php">
            <?php
                // display each schedule
                foreach($schedules as $schedule) {
                    $id = $schedule->id_schedule;
                    ?>
                        <p>ID: <?php echo $id ?></p>
                        <input type="hidden" name="id-<?php echo $id; ?>" value="<?php echo $id ?>"/>
                        <p>Name: <input type="text" name="<?php echo "name-$id"; ?>" value="<?php echo $schedule->name; ?>"></p>
                        <p>Time: <input type="text" name="<?php echo "time-$id"; ?>" value="<?php echo $schedule->time; ?>"></p>
                        <p>Line: 
                            <select name="line-id-<?php echo $id; ?>">
                                <?php
                                    // list lines
                                    foreach($lines as $line) {
                                        ?>
                                            <option value="<?php echo $line->id_line; ?>"<?php
                                                if($line->id_line === $schedule->line_id) {
                                                    echo ' selected="selected" ';
                                                }
                                            ?>>
                                                <?php echo $line->name; ?>
                                            </option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </p>
                        <p>Bus: 
                            <select name="bus-id-<?php echo $id; ?>">
                                <?php
                                    // list buses
                                    foreach($buses as $bus) {
                                        ?>
                                            <option value="<?php echo $bus->id_bus; ?>"<?php
                                                if($bus->id_bus === $schedule->bus_id) {
                                                    echo ' selected="selected" ';
                                                }
                                            ?>>
                                                <?php echo $bus->name; ?>
                                            </option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </p>
                        <p>Delete? <input type="checkbox" name="<?php echo "delete-$id"; ?>"></p>
                        <hr>
                    <?php
                }
            ?>
            <p>New entry - enter data to insert a schedule</p>
            <p>Name: <input type="text" name="name-new"></p>
            <p>Time: <input type="text" name="time-new"></p>
            <p>Lines: 
                <select name="line-id-new">
                    <?php
                        // list lines
                        foreach($lines as $line) {
                            ?>
                                <option value="<?php echo $line->id_line; ?>">
                                    <?php echo $line->name; ?>
                                </option>
                            <?php
                        }
                    ?>
                </select>
            </p>
            <p>Bus: 
                <select name="bus-id-new">
                    <?php
                        //list buses
                        foreach($buses as $bus) {
                            ?>
                                <option value="<?php echo $bus->id_bus; ?>">
                                    <?php echo $bus->name; ?>
                                </option>
                            <?php
                        }
                    ?>
                </select>
            </p>
            <hr>

            <input type="hidden" name="count" value="<?php echo get_latest_id(Bus::TABLE); ?>">
            <input type="submit" value="Apply changes" name="submit" />
            <a href="index.php">Back to home</a>
        </form>
    <?php