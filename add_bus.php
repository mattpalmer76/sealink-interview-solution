<h1>Add or remove buses</h1>

<?php
    include_once 'functions.php';
    
    connect_db();
    
    // submission logic
    if(isset($_POST['submit'])) {
        // go though each record and update
        $count = $_POST['count'];
        for($i = 0; $i <= $count; $i++) {
            if(isset($_POST['id-' .$i])) {
                $id = $_POST['id-' .$i];
                if(isset($_POST['delete-' .$id])) {
                    // attempt a delete
                    $deleted = delete($id, Bus::TABLE);
                    // if cannot delete, it's likely linked to another record
                    if(!$deleted) {
                        echo '<p>Could not delete "' .$_POST['name-' .$id] .'" (id ' .$id .'). It is likely this record is used in another record.</p>';
                    }
                } else {
                    // update data
                    Bus::upsert($_POST['name-' .$id], $_POST['model-' .$id], strval($id));
                }
            }
            
        }
        
        // for adding a new record
        if(!empty($_POST['name-new']) || !empty($_POST['model-new'])) {
            // insert new bus
            Bus::upsert($_POST['name-new'], $_POST['model-new']);
        }
    }

    // get latest array of buses
    $buses = Bus::getAllBuses();

    ?>
        <form method="post" action="add_bus.php">
            <?php
                // display each bus
                foreach($buses as $bus) {
                    $id = $bus->id_bus;
                    ?>
                        <p>ID: <?php echo $id ?></p>
                        <input type="hidden" name="id-<?php echo $id; ?>" value="<?php echo $id ?>"/>
                        <p>Registration: <input type="text" name="<?php echo "name-$id"; ?>" value="<?php echo $bus->name; ?>"></p>
                        <p>Model: <input type="text" name="<?php echo "model-$id"; ?>" value="<?php echo $bus->model; ?>"></p>
                        <p>Delete? <input type="checkbox" name="<?php echo "delete-$id"; ?>"></p>
                        <hr>
                    <?php

                }
            ?>
            <p>New entry - enter data to insert a bus</p>
            <p>Registration: <input type="text" name="name-new"></p>
            <p>Model: <input type="text" name="model-new"></p>
            <hr>

            <input type="hidden" name="count" value="<?php echo get_latest_id(Bus::TABLE); ?>">
            <input type="submit" value="Apply changes" name="submit" />
            <a href="index.php">Back to home</a>
        </form>
    <?php