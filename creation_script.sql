-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema speedco
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema speedco
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `speedco` DEFAULT CHARACTER SET utf8 ;
USE `speedco` ;

-- -----------------------------------------------------
-- Table `speedco`.`Bus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `speedco`.`Bus` (
  `id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Model` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `speedco`.`Line`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `speedco`.`Line` (
  `id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `speedco`.`Route`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `speedco`.`Route` (
  `id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Line_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Route_Line1_idx` (`Line_id` ASC),
  CONSTRAINT `fk_Route_Line1`
    FOREIGN KEY (`Line_id`)
    REFERENCES `speedco`.`Line` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `speedco`.`Stop`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `speedco`.`Stop` (
  `id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  `Coords` VARCHAR(45) NULL,
  `Route_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Stop_Route1_idx` (`Route_id` ASC),
  CONSTRAINT `fk_Stop_Route1`
    FOREIGN KEY (`Route_id`)
    REFERENCES `speedco`.`Route` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `speedco`.`Schedule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `speedco`.`Schedule` (
  `id` INT NOT NULL,
  `Time` VARCHAR(100) NULL,
  `Bus_id` INT NOT NULL,
  `Line_id` INT NOT NULL,
  `Name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Schedule_Bus1_idx` (`Bus_id` ASC),
  INDEX `fk_Schedule_Line1_idx` (`Line_id` ASC),
  CONSTRAINT `fk_Schedule_Bus1`
    FOREIGN KEY (`Bus_id`)
    REFERENCES `speedco`.`Bus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Schedule_Line1`
    FOREIGN KEY (`Line_id`)
    REFERENCES `speedco`.`Line` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- -----------------------------------------------------
-- Insert demo data
-- -----------------------------------------------------

INSERT INTO `bus` (`id`, `Name`, `Model`) VALUES
(1, 'ABC-123', 'Mann'),
(2, 'XYZ-987', 'BMW');

INSERT INTO `line` (`id`, `Name`) VALUES
(1, 'Adelaide/Modbury');

INSERT INTO `route` (`id`, `Name`, `Line_id`) VALUES
(1, 'To City', 1),
(2, 'From City', 1);

INSERT INTO `schedule` (`id`, `Time`, `Bus_id`, `Line_id`, `Name`) VALUES
(1, '8:00am', 1, 1, 'Morning route'),
(2, '4:00pm', 2, 1, 'Afternoon route');

INSERT INTO `stop` (`id`, `Name`, `Coords`, `Route_id`) VALUES
(1, '1', '25Nx25W', 1),
(2, '2', '25Nx26W', 1),
(3, '3', '25Nx27W', 1),
(4, '1a', '25Nx27W', 2),
(5, '1b', '25Nx26W', 2),
(6, '1c', '25Nx25W', 2);