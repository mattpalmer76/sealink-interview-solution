<h1>SpeedCo</h1>
<p>A basic implementation for the interview test for Sealink</p>
<hr/>

<a href="add_bus.php">Add or remove buses</a>
<br/>
<a href="add_stop.php">Add or remove stops</a>
<br/>
<a href="add_route.php">Add or remove routes</a>
<br/>
<a href="add_line.php">Add or remove lines</a>
<br/>
<a href="add_schedule.php">Schedule lines to a bus for a given time</a>
<br/>

<?php
    include_once 'functions.php';

    // on page load, connect to db (and instantiate db if need be)
    connect_db();
