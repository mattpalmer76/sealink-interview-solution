<h1>Add or remove lines</h1>

<?php
    include_once 'functions.php';
    
    connect_db();

    // submission logic
    if(isset($_POST['submit'])) {
        // go though each record and update
        $count = $_POST['count'];
        for($i = 0; $i <= $count; $i++) {
            if(isset($_POST['id-' .$i])) {
                $id = $_POST['id-' .$i];
                if(isset($_POST['delete-' .$id])) {
                    // attempt a delete
                    $deleted = delete($id, Line::TABLE);
                    // if cannot delete, it's likely linked to another record
                    if(!$deleted) {
                        echo '<p>Could not delete "' .$_POST['name-' .$id] .'" (id ' .$id .'). It is likely this record is used in another record.</p>';
                    }
                } else {
                    // update data
                    Line::upsert($_POST['name-' .$id], strval($id));
                }
            }
            
        }
        
        // for adding a new record
        if(!empty($_POST['name-new'])) {
            // insert new line
            Line::upsert($_POST['name-new']);
        }
    }

    // get latest array of lines
    $lines = Line::getAllLines();

    ?>
        <form method="post" action="add_line.php">
            <?php
                // display each line
                foreach($lines as $line) {
                    $id = $line->id_line;
                    ?>
                        <p>ID: <?php echo $id ?></p>
                        <input type="hidden" name="id-<?php echo $id; ?>" value="<?php echo $id; ?>"/>
                        <p>Name: <input type="text" name="<?php echo "name-$id"; ?>" value="<?php echo $line->name; ?>"></p>
                        <p>Delete? <input type="checkbox" name="<?php echo "delete-$id"; ?>"></p>
                        <hr>
                    <?php

                }
            ?>
            <p>New entry - enter data to insert a line</p>
            <p>Name: <input type="text" name="name-new"></p>
            <hr>

            <input type="hidden" name="count" value="<?php echo get_latest_id(Line::TABLE); ?>">
            <input type="submit" value="Apply changes" name="submit" />
            <a href="index.php">Back to home</a>
        </form>
    <?php


