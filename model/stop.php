<?php
    include_once('./database_helper.php');

    class Stop {
        public const ID_STOP = 'id';
        public const NAME = 'Name';
        public const COORDS = 'Coords';
        public const ROUTE_ID = 'Route_id';

        public const TABLE = "Stop";

        public $id_stop = null;
        public $name = null;
        public $coords = null;
        public $route_id = null;

        private $db_helper = null;

        public function __construct($id, $name, $coords, $route_id) {
            $this->id_stop = $id;
            $this->name = $name;
            $this->coords = $coords;
            $this->route_id = $route_id;

            $this->db_helper = new DatabaseHelper();
        }

        // get all stops in the array
        public static function getAllStops() {
            $results = array();

            $result = $GLOBALS["dh"]->conn->query('SELECT * FROM STOP');

            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    $id = $row[Stop::ID_STOP];
                    $results[] = new Stop($id, $row[Stop::NAME], $row[Stop::COORDS], $row[Stop::ROUTE_ID]);
                }
            }

            return $results;
        }

        // store a stop in the database
        public static function upsert($name, $coords, $route_id, $id = "") {
            $statement = null;

            if($id === "") {
                $id = $GLOBALS["dh"]->last_id(Stop::TABLE);
                $id++;
                $statement = "INSERT INTO " .Stop::TABLE 
                    ." (`" .Stop::ID_STOP ."`, `" .Stop::NAME ."`, `" .Stop::COORDS  ."`, `"  .Stop::ROUTE_ID
                    ."`) VALUES ('$id', '$name', '$coords', '$route_id')";
            } else {
                // check record exists
                if(check_record_exists($id, Stop::TABLE)) {
                    // update
                    $statement = "UPDATE `" .Stop::TABLE ."` SET `"
                        .Stop::NAME ."` = '$name', `"
                        .Stop::COORDS ."` = '$coords', `"
                        .Stop::ROUTE_ID ."` = $route_id "
                        ." WHERE `" .Stop::ID_STOP ."` = $id";
                } else {
                    $statement = "INSERT INTO " .Stop::TABLE 
                    ." (`" .Stop::ID_STOP ."`, `" .Stop::NAME ."`, `" .Stop::COORDS  ."`, `"  .Stop::ROUTE_ID
                    ."`) VALUES ('$id', '$name', '$coords', '$route_id')";
                }
            }

            $result = $GLOBALS["dh"]->conn->query($statement);

            return $result === true;
        }
    }