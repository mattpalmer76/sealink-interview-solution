<?php
    include_once('./database_helper.php');

    class Schedule {
        public const ID_SCHEDULE = "id";
        public const TIME = "Time";
        public const BUS_ID = "Bus_id";
        public const LINE_ID = "Line_id";
        public const NAME = "Name";

        public const TABLE = "SCHEDULE";

        public $id_schedule = null;
        public $time = null;
        public $bus_id = null;
        public $line_id = null;
        public $name = null;

        private $db_helper = null;

        public function __construct($id, $time, $bus_id, $line_id, $name) {
            $this->id_schedule = $id;
            $this->time = $time;
            $this->bus_id = $bus_id;
            $this->line_id = $line_id;
            $this->name = $name;

            $this->db_helper = new DatabaseHelper();
        }

        // retrieve all schedules in the array
        public static function getAllSchedules() {
            $results = array();

            $result = $GLOBALS["dh"]->conn->query('SELECT * FROM SCHEDULE');

            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    
                    $id = $row[Schedule::ID_SCHEDULE];
                    $results[] = new Schedule($id, $row[Schedule::TIME], $row[Schedule::BUS_ID], $row[Schedule::LINE_ID], $row[Schedule::NAME]);
                }
            }

            return $results;
        }

        // store a schedule to database
        public static function upsert($time, $bus_id, $line_id, $name, $id = "") {
            $statement = null;
            
            if($id === "") {
                $id = $GLOBALS["dh"]->last_id(Schedule::TABLE);
                $id++;
                $statement = "INSERT INTO " .Schedule::TABLE 
                    ." (`" .Schedule::ID_SCHEDULE ."`, `" .Schedule::TIME ."`, `" .Schedule::BUS_ID ."`, `" .Schedule::LINE_ID ."`, `" .Schedule::NAME 
                    ."`) VALUES ('$id', '$time', '$bus_id', '$line_id', '$name')";
            } else {
                // check if record exists
                if(check_record_exists($id, Schedule::TABLE)) {
                    // update that record
                    $statement = "UPDATE " .Schedule::TABLE . " SET `" 
                        .Schedule::TIME ."` = '$time' , `" 
                        .Schedule::BUS_ID ."` = '$bus_id' , `" 
                        .Schedule::LINE_ID ."` = '$line_id' , `" 
                        .Schedule::NAME ."` = '$name' WHERE `" .Schedule::ID_SCHEDULE ."` = '$id'";
                } else {
                    // if not, create
                    $statement = "INSERT INTO " .Schedule::TABLE 
                        ." (`" .Schedule::ID_SCHEDULE ."`, `" .Schedule::TIME ."`, `" .Schedule::BUS_ID ."`, `" .Schedule::LINE_ID ."`, `" .Schedule::NAME 
                        ."`) VALUES ('$id', '$time', '$bus_id', '$line_id', '$name')";
                }
            }

            $result = $GLOBALS["dh"]->conn->query($statement);

            return $result === true;
        }

        // check if record exists
        private static function check_record_exists($id) {
            $result = $GLOBALS["dh"]->conn->query("SELECT * FROM SCHEDULE WHERE `ID` = $id");

            return $result->num_rows > 0;
        }

        // delete a schedule
        public static function delete($id) {
            $statement = "DELETE FROM " .Schedule::TABLE ." WHERE `id` = $id";
            $result = $GLOBALS["dh"]->conn->query($statement);
            return $result === true;
        }
    }