<?php
    include_once('./database_helper.php');

    class Route {
        public const ID_ROUTE = 'id';
        public const NAME = "Name";
        public const LINE_ID = "Line_id";

        public const TABLE = "ROUTE";

        public $id_route = null;
        public $name = null;
        public $line_id = null;

        private $db_helper = null;

        public function __construct($id, $name, $line_id) {
            $this->id_route = $id;
            $this->name = $name;
            $this->line_id = $line_id;

            $this->db_helper = new DatabaseHelper();
        }

        // get all routes in the array
        public static function getAllRoutes() {
            $results = array();

            $result = $GLOBALS["dh"]->conn->query('SELECT * FROM ROUTE');

            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    $id = $row[Route::ID_ROUTE];
                    $results[] = new Route($id, $row[Route::NAME], $row[Route::LINE_ID]);
                }
            }

            return $results;
        }

        // store a route to the database
        public static function upsert($name, $line_id, $id = "") {

            $statement = null;

            if($id === "") {
                $id = $GLOBALS["dh"]->last_id(Route::TABLE);
                $id++;
                $statement = "INSERT INTO " .Route::TABLE 
                    ." (`" .Route::ID_ROUTE ."`, `"  .Route::NAME  ."`, `"  .Route::LINE_ID
                    ."`) VALUES ('$id', '$name', '$line_id')";
            } else {
                // check if record exists
                if(check_record_exists($id, Route::TABLE)) {
                    // update that record
                    $statement = "UPDATE " .Route::TABLE . " SET `" 
                    .Route::NAME ."` = '$name', `" 
                    .Route::LINE_ID ."` = '$line_id' "
                    ." WHERE `" .Route::ID_ROUTE ."` = '$id'";
                } else {
                    $statement = "INSERT INTO " .Route::TABLE 
                    ." (`" .Route::ID_ROUTE ."`, `"  .Route::NAME  ."`, `"  .Route::LINE_ID
                    ."`) VALUES ('$id', '$name', '$line_id')";
                }
            }

            $result = $GLOBALS["dh"]->conn->query($statement);


            return $result === true;
        }
        
        // check if record exists
        private static function check_record_exists($id) {
            $result = $GLOBALS["dh"]->conn->query("SELECT * FROM ROUTE WHERE `ID` = $id");

            return $result->num_rows > 0;
        }

        // delete a line
        public static function delete($id) {
            $statement = "DELETE FROM " .Route::TABLE ." WHERE `id` = $id";
            $result = $GLOBALS["dh"]->conn->query($statement);
            return $result === true;
        }
    }