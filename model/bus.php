<?php
    include_once('./database_helper.php');

    class Bus {
        public const ID_BUS = "id";
        public const NAME = "Name";
        public const MODEL = "Model";
        public const TABLE = "BUS";

        public $id_bus = null;
        public $name = null;
        public $model = null;

        private $db_helper = null;

        public function __construct($id_bus, $name, $model) {
            $this->id_bus = $id_bus;
            $this->name = $name;
            $this->model = $model;

            $this->db_helper = new DatabaseHelper();
        }

        // retrieve all buses in an array
        public static function getAllBuses() {
            $buses = array();

            $result = $GLOBALS["dh"]->conn->query('SELECT * FROM BUS');

            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    
                    $id = $row[Bus::ID_BUS];
                    $buses[] = new Bus($id, $row[Bus::NAME], $row[Bus::MODEL]);
                }
            }

            return $buses;
        }

        // store a bus to database
        public static function upsert($name = "unnamed", $model = "no model", $id = "") {
            $statement = null;
            
            if($id === "") {
                $id = $GLOBALS["dh"]->last_id(Bus::TABLE);
                $id++;
                $statement = "INSERT INTO " .Bus::TABLE ." (`" .Bus::ID_BUS ."`, `" .Bus::NAME ."`, `" .BUS::MODEL ."`) VALUES ('$id', '$name', '$model')";
            } else {
                // check if record exists
                if(check_record_exists($id, Bus::TABLE)) {
                    // update that record
                    $statement = "UPDATE " .Bus::TABLE . " SET `" .Bus::NAME ."` = '$name' , `" .BUS::MODEL ."` = '$model' WHERE `" .Bus::ID_BUS ."` = '$id'";
                } else {
                    // if not, create
                    $statement = "INSERT INTO " .Bus::TABLE ." (`" .Bus::ID_BUS ."`, `" .Bus::NAME ."`, `" .BUS::MODEL ."`) VALUES ('$id', '$name', '$model')";
                }
            }

            $result = $GLOBALS["dh"]->conn->query($statement);

            return $result === true;
        }

        // check if record exists
        private static function check_record_exists($id) {
            $result = $GLOBALS["dh"]->conn->query("SELECT * FROM BUS WHERE `ID` = $id");

            return $result->num_rows > 0;
        }

        // delete a bus
        public static function delete($id) {
            $statement = "DELETE FROM " .Bus::TABLE ." WHERE `id` = $id";
            $result = $GLOBALS["dh"]->conn->query($statement);
            return $result === true;
        }
    }