<?php
    include_once('./database_helper.php');

    class Line {
        public const ID_LINE = "id";
        public const NAME = "Name";
        public const TABLE = "Line";

        public $id_line = null;
        public $name = null;

        private $db_helper = null;

        public function __construct($id, $name) {
            $this->id_line = $id;
            $this->name = $name;

            $this->db_helper = new DatabaseHelper();
        }

        // get all lines in the array
        public static function getAllLines() {
            $results = array();

            $result = $GLOBALS["dh"]->conn->query('SELECT * FROM LINE');

            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    
                    $id = $row[Line::ID_LINE];
                    $results[] = new Line($id, $row[Line::NAME]);
                }
            }

            return $results;
        }

        // store a line to database
        public static function upsert($name, $id = "") {
            $statement = null;
            
            if($id === "") {
                $id = $GLOBALS["dh"]->last_id(Line::TABLE);
                $id++;
                $statement = "INSERT INTO " .Line::TABLE 
                    ." (`" .Line::ID_LINE ."`, `"  .Line::NAME 
                    ."`) VALUES ('$id', '$name')";
            } else {
                // check if record exists
                if(check_record_exists($id, Line::TABLE)) {
                    // update that record
                    $statement = "UPDATE " .Line::TABLE . " SET `" 
                        .Line::NAME ."` = '$name' WHERE `" .Line::ID_LINE ."` = '$id'";
                } else {
                    // if not, create
                    $statement = "INSERT INTO " .Line::TABLE 
                        ." (`" .Line::ID_LINE ."`, `"  .Line::NAME 
                        ."`) VALUES ('$id', '$name')";
                }
            }

            $result = $GLOBALS["dh"]->conn->query($statement);

            return $result === true;
        }

         // check if record exists
         private static function check_record_exists($id) {
            $result = $GLOBALS["dh"]->conn->query("SELECT * FROM LINE WHERE `ID` = $id");

            return $result->num_rows > 0;
        }

        // delete a schedule
        public static function delete($id) {
            $statement = "DELETE FROM " .Line::TABLE ." WHERE `id` = $id";
            $result = $GLOBALS["dh"]->conn->query($statement);
            return $result === true;
        }
    }