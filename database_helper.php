<?php
    class DatabaseHelper {
        // connection deets
        const SERVER_NAME = "localhost";
        const USERNAME = "root";
        const PASSWORD = "";
        const DATABASE = "speedco";

        // name of creation script
        const CREATION_SCRIPT_FILENAME = "creation_script.sql";

        public $conn = null;

        public function __construct() {
            $this->create_database_if_not_exist();
        }

        // for getting/creating database if need be
        public function create_database_if_not_exist() {
            
            if(empty($this->conn)) {
                // connect or create database if empty

                // first we connect to see if the database is present
                $this->conn = mysqli_connect(self::SERVER_NAME, self::USERNAME, self::PASSWORD);

                // if a problem connecting, die
                if($this->conn->connect_error) {
                    die('There was a problem connecting to the database.');
                }

                // check for speedco database
                $query = 'SHOW DATABASES LIKE "' . self::DATABASE . '"';
                $get_dbs = $this->conn->query($query);

                if($get_dbs->num_rows == 1) {
                    // the database exists, just connect to it
                    $this->conn = mysqli_connect(self::SERVER_NAME, self::USERNAME, self::PASSWORD, self::DATABASE);
                } else {
                    // load creation script and execute
                    $db = new PDO('mysql:host=' .self::SERVER_NAME, self::USERNAME, self::PASSWORD);
                    $sql = file_get_contents(self::CREATION_SCRIPT_FILENAME);
                    $qr = $db->exec($sql);

                    $this->conn = mysqli_connect(self::SERVER_NAME, self::USERNAME, self::PASSWORD, self::DATABASE);
                }
                return $this->conn;

            } else {
                // the database connection already exists and is up
                return $this->conn;
            }
        }

        // get the last id
        public function last_id($table) {
            $query = "SELECT MAX(`id`) AS `id` FROM `$table`";
            $result = $this->conn->query($query);

            if(!empty($result) && $result->num_rows == 1) {
                $row = $result->fetch_assoc();
                return $row['id'];
            } else {
                return null;
            }
            
        }

    }